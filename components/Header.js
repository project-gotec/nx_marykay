import { useEffect, useState } from 'react'
import client from '../utils/client'
import { useRouter } from 'next/router'
import Link from 'next/link';

export default function Header() {
    
    const router = useRouter()
    
    const [data, setData] = useState([]);

    useEffect(() => {
        const credentials = localStorage.getItem('credentials')

        if(!credentials) {
            router.push('/noauth')
        }
        else {
            client.get(`/auth?string=${credentials}`)
            .then(res => {
                if(res.data.success) {
                    const stringData = res.data.credentials
                    setData(stringData.split('|'))
                } else {
                    router.push('/noauth')
                }
                
            })
            .catch(err => {
                console.log(err)
                router.push('/noauth')
            })
        }
      }, [])


     const handleLogout = () => {
          localStorage.removeItem('credentials')
          router.push('/')
      }

    return (
        <div className="bg-light shadow">
            <div className="container d-flex justify-content-between align-items-center p-2">
                <Link href="/hall">
                    <a>
                        <img src="/images/logotipo.png" className="img-fluid" width="150"/>
                    </a>
                </Link>
                <div className="d-flex justify-content-between align-items-baseline">
                    <h6 className="pr-3">{data[1]}</h6>
                    <button onClick={handleLogout} className="btn-pink">Sair</button>
                </div>    
            </div>
        </div>
    )
}