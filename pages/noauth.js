import { useRouter } from "next/router"
import { useEffect } from "react"

export default function naoAutenticado() {  
    const router = useRouter()

    return (
        <div className="container text-center">
            <h1>Você não está logado.</h1>
        </div>
    )
}