const crypto = require('crypto')

export default function handler(req, res) {

  if(req.method === 'GET') {
    const {string} = req.query
    const key = process.env.AES_KEY
    const iv = process.env.AES_IV
    const mode = process.env.AES_MODE

    try {
      const decipher = crypto.createDecipheriv(mode, key, iv)
      const decrypted = decipher.update(string, 'base64', 'utf-8') + decipher.final('utf-8')  
      
      res.status(200).json({credentials: decrypted, success: true})
    }
    catch (err) {
      res.status(401).json({success: false})
    }
    
    // URL para autenticação teste: 
    //http://localhost:3000/?string=3f22f3KlnB79uASFUZUhk3/sY2yzD2IzH57YtG31uRlh4p7acAjvmQKak3pc48BO
  }
  else {
    res.status(404).json({success: false})
  }
}
