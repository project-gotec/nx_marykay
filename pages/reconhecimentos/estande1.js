import Link from 'next/link';
import Header from '../../components/Header';

export default function Estande1() {


  return (
  
    <div className="page">
       <Header />
       <svg viewBox="0 0 2000 1126" preserveAspectRatio="none">

          <Link href="#">
              <a>
                <polygon className="polygon" points="391.5,229.5 391.5,411.5 910.5,411.5 910.5,229.5 391.5,228.5 "/>
              </a>
          </Link>

          <Link href="#">
              <a>
              <polygon className="polygon" points="952.5,210.5 952.5,546.5 952.5,547.5 1643.5,548.5 1628.5,210.5 "/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <path className="polygon" d="M591.5,630.5c0,0-20,2-17,17l-9,299c0,0,1,15,13,19l143-2c0,0,11-5,9-21l5-301c0,0-3-13-21-11H591.5z"/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <polygon className="polygon" points="800.5,652.5 800.5,842.5 1180.5,842.5 1178.5,651.5 "/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <polygon className="polygon" points="1190.5,652.5 1192.5,818.5 1194.5,938.5 1194.5,941.5 1484.5,938.5 1477.5,652.5 "/>
              </a>
          </Link>

        </svg>

      <img src="/images/reconhecimentos/MK_RECONHECIMENTO_estande-01.jpg" className="img-background" />
      
    </div>
  )


}

