import Link from 'next/link';
import Header from '../../components/Header';

export default function Estande2() {

  return (
  
    <div className="page">
       <Header />
       <svg viewBox="0 0 2000 1126" preserveAspectRatio="none">
          <Link href="#">
              <a>
                <polygon className=" polygon
                " points="370.5,210.5 357.5,549.5 1615.5,548.5 1607.5,210.5 372.5,210.5" style={{zIndex: 0, backgroundColor: 'transparent'}} />

                <foreignObject x="356" y="209.5" width="1265px" height="340px" style={{borderRadius: 8, backgroundColor: 'transparent', borderTopLeftRadius: 28, borderTopRightRadius: 22}}>
                  <div className="bg-secondary d-flex justify-content-center">
                    <iframe id="ytplayer" type="text/html" width="100%" height="340px"
                    allow='autoplay'
                    src="https://www.youtube.com/embed/eCt-8PT75xM?rel=0&modestbranding=0&autohide=1&mute=1&showinfo=0&controls=0&autoplay=1"
                    frameBorder="0"/>
                  </div>
                </foreignObject>

              </a>
          </Link>

          <Link href="#">
              <a>
                <polygon className="polygon
                " points="523.5,653.5 514.5,938.5 805.5,940.5 809.5,651.5 "/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <polygon className="polygon
                " points="820.5,651.5 819.5,939.5 1200.5,939.5 1200.5,651.5 "/>
              </a>
          </Link>

          <Link href="#">
              <a>
              <path className="polygon
              " d="M1278.5,630.5c0,0-14,0-12,22l3,294c0,0,5,23,19,18l132,1c0,0,17-6,15-25l-8-295c0,0-9-18-19-15
	            S1278.5,630.5,1278.5,630.5z"/>
              </a>
          </Link>

        </svg>

      <img src="/images/reconhecimentos/MK_RECONHECIMENTO_estande-02.jpg" className="img-background" />
      
    </div>
  )


}

