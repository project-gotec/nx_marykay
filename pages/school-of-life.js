import Link from 'next/link';
import Header from '../components/Header';

export default function schoolOfLife() {

    return (
        <>
            <Header />
            <section className="bg-pink-white w-100 bg-tools p-5" style={{overflow:'hidden'}}>
                <div className="container">
                    <header className="d-flex justify-content-center ml-4">
                            <img src="/images/school/logo.png" className="img-fluid" width='250' ></img>
                    </header>
                </div>
                
            <div className="container d-flex justify-content-center flex-wrap mt-4">

                <div className="card bg-light-pink" style={{maxWidth: '250px'}}>
                    <img src="https://dummyimage.com/300x350/fff/aaa" className="card-img-top p-3 bg-light-pink" width='180'/> 
                    <div className="card-body bg-light-pink">
                        <div className="d-flex justify-content-start flex-wrap">
                            <h5 className="font-weight-bold mt-n4">Palestrante</h5>
                            <small className="w-100">Titulo</small>
                        </div>
                        <p className="text-pink mt-3 font-weight-bold text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <div className="d-flex justify-content-center align-self-end">
                            <a href="#" className="btn-pink">ACESSAR</a>
                        </div>
                    </div>
                </div>

                <div className="card bg-lighter-pink" style={{maxWidth: '250px'}}>
                    <img src="https://dummyimage.com/300x350/fff/aaa" className="card-img-top p-3 bg-lighter-pink" width='180'/> 
                    <div className="card-body bg-lighter-pink">
                        <div className="d-flex justify-content-start flex-wrap">
                            <h5 className="font-weight-bold mt-n4">Palestrante</h5>
                            <small className="w-100">Titulo</small>
                        </div>
                        <p className="text-pink mt-3 font-weight-bold text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <div className="d-flex justify-content-center align-self-end">
                            <a href="#" className="btn-pink">ACESSAR</a>
                        </div>
                    </div>
                </div>

                <div className="card bg-light-pink" style={{maxWidth: '250px'}}>
                    <img src="https://dummyimage.com/300x350/fff/aaa" className="card-img-top p-3 bg-light-pink" width='180'/> 
                    <div className="card-body bg-light-pink">
                        <div className="d-flex justify-content-start flex-wrap">
                            <h5 className="font-weight-bold mt-n4">Palestrante</h5>
                            <small className="w-100">Titulo</small>
                        </div>
                        <p className="text-pink mt-3 font-weight-bold text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <div className="d-flex justify-content-center align-self-end">
                            <a href="#" className="btn-pink">ACESSAR</a>
                        </div>
                    </div>
                </div>

                <div className="card bg-lighter-pink" style={{maxWidth: '250px'}}>
                    <img src="https://dummyimage.com/300x350/fff/aaa" className="card-img-top p-3 bg-lighter-pink" width='180'/> 
                    <div className="card-body bg-lighter-pink">
                        <div className="d-flex justify-content-start flex-wrap">
                            <h5 className="font-weight-bold mt-n4">Palestrante</h5>
                            <small className="w-100">Titulo</small>
                        </div>
                        <p className="text-pink mt-3 font-weight-bold text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                        <div className="d-flex justify-content-center align-self-end">
                            <a href="#" className="btn-pink">ACESSAR</a>
                        </div>
                    </div>
                </div>
    
            </div>

            </section>
        </>
        
    );
}
