import Link from 'next/link';
import Header from '../components/Header';

export default function Plenaria() {
  return (
  
    <div className="page">
       <Header />
       <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">

          <Link href="#">
              <a>
              <path className="polygon" d="M1480.5,679.5l3-24l4-18l-6-1v-2h2v-3l-17-16l-3-4v-5l-2-3l1-1l2,2l5,2l3,4l11,11l2-2c0,0,2-2,2-3s4-10,4-10
                l6-3l1-1l-2-2h-4h-1v-3l-1-2l-1-1l-1-1l1-2l1-1l1-3l1-8c0,0,12-7,20,1l4,10l-1,1l-2,2v3v2h-3h-2l1,2l4,2l6,4l4,6v5v11l-1,1v4v2l-1,1
                l-1,3h-2l-2,2l2,3v3l1,3l1,7h-2l-1,4l1,3c0,0,11-13,21-2l-16-365l-1042-2h-2v2l-17,382h96c0,0,13-14,21,0h8c0,0,0-4-2-6s-5-15,9-16
                c0,0,10-4,12,8l1,14h78c0,0,12-8,28,0h38c0,0,11-20,30-6c0,0-3-6,8-8c0,0,14-4,16,13l13,1c0,0-1-28,19-19c0,0,9-4,9,18l26,1
                c0,0,7-32,29-12c0,0-1-11,12-9c0,0,11-1,8,11c0,0,5,4-1,9l38,1h235h1c0,0-2-20,13-18c0,0,16,0,14,17l12,1l1-13c0,0,13-15,21-1l3,8v6
                h48l8-4v-9c0,0,6-14,14-10c0,0,10-2,9,17c0,0,14-7,19,1c0,0-2-16,13-14c0,0,15,0,13,15l1,4h1l2-15c0,0,7-8,12-4c0,0,13-1,10,19h28
                c0,0,1-16,16-14c0,0,15-3,18,15"/>
              </a>
          </Link>

        </svg>

      <img src="/images/MK_PLENARIA.jpg" className="img-background" />
      
    </div>
  )


}
