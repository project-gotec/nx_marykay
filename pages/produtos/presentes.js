import Link from 'next/link';
import Header from '../../components/Header';

export default function Presentes() {
  return (
  
    <div className="page">
        <Header />
       <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">

          <Link href="#">
              <a>
              <path className="polygon" d="M175.5,255.5v566l349,10l-2-73l-3-1v-28h5v-145l-7-2v-29l7,1v-146h-7l-1-30h5l-3-133l-72,3c0,0-6,18-19,2l-5-1
	            l-2,2h-7l-1-2l-109,4l-1,2h-8l-1-3l-40,2c0,0-5,13-18,1H175.5z"/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <path className="polygon" d="M679.5,241.5v244c0,0,24,19,7,50c0,0-2,4,32,15l2,2l14,98l-5,22l-1,7l-14,5l4,30l-4,2v121v2l505,15v-625h-45
	            c0,0-3,21-22,1l-203,5c0,0,0,15-18,5l-1-4l-197,4c0,0,0,10-8,8c0,0-6,3-10-7H679.5z"/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <path className="polygon" d="M1254.5,227.5l-3,629l421,12l7-650l-82,2c0,0-8,17-20,0c0,0-221,6-221,6s-7,14-18,0l-10-1l-4,13c0,0-8,2-9-11
	            H1254.5z"/>
              </a>
          </Link>

        </svg>

      <img src="/images/produtos/MK_PRODUTOS_estande-presentes.jpg" className="img-background" />
      
    </div>
  )


}
