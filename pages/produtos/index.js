import Link from 'next/link';
import Header from '../../components/Header';

export default function IndexProdutos() {


  return (
  
    <div className="page">
       <Header />
       <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">

          <Link href="/produtos/makeup">
              <a>
                <path className="polygon" d="M122.5,910.5l178,15c0,0-8-9,8-10l183-269v-10l-109-7l-34-124h-25l-263,264L122.5,910.5z"/>
              </a>
          </Link>

          <Link href="/produtos/presentes">
              <a>
                <path className="polygon" d="M455.5,428.5l27,123l29,58l48-15l1-4l2-2l-1-1l-1,1l-3,1l-1-1v-15c0,0,3-3,5-2v-11c0,0,6-11,13-1v5l2,3l1,1v1v2
	            v1l3,4l1,3v9l155-42l-2-2v-4l4-2v-3l-5-1v-9v-1c0,0,17-5,15,11v2l4,1l1,4l99-27l-42-48l-8-106l-7-8l-340,73V428.5z"/>
              </a>
          </Link>

          <Link href="/produtos/skin-care">
              <a>
                <polygon className="polygon" points="1238.5,355.5 1229.5,363.5 1219.5,471.5 1177.5,516.5 1520.5,614.5 1548.5,559.5 1554.5,545.5 
	            1580.5,434.5 1580.5,432.5 "/>
              </a>
          </Link>

          <Link href="/produtos/fragrancias">
              <a>
                <path className="polygon" d="M1521.5,643.5l118-5l34-123l28-1l260,272v1l-66,142l-176,10c0,0,2-23-7-19v-10c0,0-5-18-15,0L1521.5,643.5z"/>
              </a>
          </Link>

        </svg>

      <img src="/images/produtos/MK_PRODUTOS.jpg" className="img-background" />
      
    </div>
  )


}

