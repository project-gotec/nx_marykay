import Link from 'next/link';
import Header from '../../components/Header';

export default function Makeup() {
  return (
  
    <div className="page">
      <Header />
       <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">

          <Link href="#">
              <a>
              <path className="polygon" d="M175.5,255.5v566l349,10l-2-73l-3-1v-28h5v-145l-7-2v-29l7,1v-146h-7l-1-30h5l-3-133l-72,3c0,0-6,18-19,2l-5-1
	            l-2,2h-7l-1-2l-109,4l-1,2h-8l-1-3l-40,2c0,0-5,13-18,1H175.5z"/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <path className="polygon" d="M679.5,241.5v244c0,0,24,19,7,50c0,0-2,4,32,15l2,2l14,98l-5,22l-1,7l-14,5l4,30l-4,2v121v2l505,15v-625h-45
	            c0,0-3,21-22,1l-203,5c0,0,0,15-18,5l-1-4l-197,4c0,0,0,10-8,8c0,0-6,3-10-7H679.5z"/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <path className="polygon" d="M1254.5,227.5l71-1l5,7l3-1l1-6h5c0,0,0,10,10,7c0,0,6,2,8-7h4l5,3l2-3l15-1c0,0-1,24,10,22h2l1-23h91
	            c0,0-5,17,6,21h2l11-23h73c0,0-3,8,9,5c0,0,2,5,9-6l51-1c0,0-13,16-3,25c0,0,11,13,22-9l8-18h3l-5,651l-424-14L1254.5,227.5z"/>
              </a>
          </Link>

        </svg>

      <img src="/images/produtos/MK_PRODUTOS_estande-makeup.jpg" className="img-background" />
      
    </div>
  )


}
