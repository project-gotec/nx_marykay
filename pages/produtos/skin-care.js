import Link from 'next/link';
import Header from '../../components/Header';

export default function SkinCare() {
  return (
  
    <div className="page">
      <Header />
       <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">

          <Link href="#">
              <a>
              <path className="polygon" d="M175.5,255.5v566l349,10l-2-73l-3-1v-28h5v-145l-7-2v-29l7,1v-146h-7l-1-30h5l-3-133l-72,3c0,0-6,18-19,2l-5-1
	            l-2,2h-7l-1-2l-109,4l-1,2h-8l-1-3l-40,2c0,0-5,13-18,1H175.5z"/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <path className="polygon" d="M679.5,241.5v244c0,0,24,19,7,50c0,0-2,4,32,15l2,2l14,98l-5,22l-1,7l-14,5l4,30l-4,2v121v2l505,15v-625h-45
	            c0,0-3,21-22,1l-203,5c0,0,0,15-18,5l-1-4l-197,4c0,0,0,10-8,8c0,0-6,3-10-7H679.5z"/>
              </a>
          </Link>

          <Link href="#">
              <a>
                <path className="polygon" d="M1253.5,226.5l-2,631l422,11l6-650h-8l-6,22c0,0-9,0-7-14l1-8l-62,2c0,0-7,15-20,1l-220,5c0,0-6,15-19,0H1253.5
	            z"/>
              </a>
          </Link>

        </svg>

      <img src="/images/produtos/MK_PRODUTOS_estande-skincare.jpg" className="img-background" />
      
    </div>
  )


}
