import Link from 'next/link';
import { useState } from 'react';
import Header from '../components/Header';


export default function orientacoesMentoras() {

    const [openVideo, setOpenVideo] = useState(false)
    const [linkVideo, setLinkVideo] = useState(null)

    const showVideo = link => {
        setOpenVideo(true)
        setLinkVideo(link)
    }

    return (
        <>
        {
            openVideo? (
                <>
                    <Header />
                    <div className="embed-responsive embed-responsive-16by9" style={{height: '94.5vh'}}>
                        <button className="btn-pink-small rounded-circle" onClick={() => setOpenVideo(false)} style={{zIndex: 1, position: 'fixed', top: '6%', marginLeft: '0.7%'}}><i className="bi bi-x" style={{fontSize: 28}}></i></button>
                        <iframe className="embed-responsive-item" src={linkVideo}></iframe>
                    </div>
                </>
            ) : (
                <>
                <Header />
                <section className="bg-pink-white w-100 bg-coracao p-5" style={{overflow:'hidden'}}>
                    <div className="container">
                        <header className="d-flex justify-content-center align-items-center p-3 text-center flex-wrap">
                                <img src="/images/logo.png" className="img-fluid mr-2 mr-md-5" width='130' ></img>
                                <h1 className="title-orientacao d-block font-weight-bold">Orientações<span className='d-block'> Mentoras</span></h1>
                        </header>
                    </div>    

                    <div className="container d-flex justify-content-center flex-wrap mt-2">

                        <div className="card bg-light-pink" style={{maxWidth: '250px'}}>
                            <img src="/images/orientacoes-mentoras/eloisa.jpg" className="card-img-top p-3 bg-light-pink" width='180'/> 
                            <div className="card-body bg-light-pink d-flex justify-content-center flex-wrap">
                                <div>
                                    <h5 className="font-weight-normal mt-n4">Eloisa <span className="font-weight-bold">Johnson</span></h5>
                                    <small>Diretora Nacional Executiva Elite de Vendas Independente  </small>
                                </div>
                                <p class="text-pink mt-3 font-weight-bold text-center">Liderar com o coração - O Verdadeiro papel da Diretora Nacional. O que fazer para ser a Verdadeira representante de Mary Kay Ash. ( Liderança servidora: Ser exemplo x problemas operacionais)</p>
                                <div className="d-flex justify-content-center align-self-end">
                                    <a href="#" className="btn-pink" onClick={() => showVideo('https://www.youtube.com/embed/ScMzIvxBSi4')}>ACESSAR</a>
                                </div>
                            </div>
                        </div>

                        <div className="card bg-lighter-pink" style={{maxWidth: '250px'}}>
                            <img src="/images/orientacoes-mentoras/clarissa.jpg" className="card-img-top p-3 bg-lighter-pink" width='180'/> 
                            <div className="card-body bg-lighter-pink d-flex justify-content-center flex-wrap">
                                <div>
                                    <h5 className="font-weight-normal mt-n4">Clarissa <span className="font-weight-bold">Viana</span></h5>
                                    <small>Diretora Nacional de Vendas Independente </small>
                                </div>
                                <p className="text-pink mt-3 font-weight-bold text-center">O que fiz pra ser! Estratégias da Diretora que aumentou consideravelmente os bônus em 2020</p>
                                <div className="d-flex justify-content-center align-self-end">
                                    <a href="#" className="btn-pink">ACESSAR</a>
                                </div>
                            </div>
                        </div>

                        <div className="card bg-light-pink" style={{maxWidth: '250px'}}>
                            <img src="/images/orientacoes-mentoras/deise.jpg" className="card-img-top p-3 bg-light-pink" width='180'/> 
                            <div className="card-body bg-light-pink d-flex justify-content-center flex-wrap">
                                <div>
                                    <h5 className="font-weight-normal mt-n4">Deise <span className="font-weight-bold">Ferreira</span></h5>
                                    <small>Diretora Nacional Sênior de Vendas Independente </small>
                                </div>
                                <p className="text-pink mt-3 font-weight-bold text-center">Amiga dos números! Quais indicadores mais relevantes e como desenvolver resultados consistentes </p>
                                <div className="d-flex justify-content-center align-self-end">
                                    <a href="#" className="btn-pink">ACESSAR</a>
                                </div>
                            </div>
                        </div>

                        <div className="card bg-lighter-pink" style={{maxWidth: '250px'}}>
                            <img src="/images/orientacoes-mentoras/karla.jpg" className="card-img-top p-3 bg-lighter-pink" width='180'/> 
                            <div className="card-body bg-lighter-pink d-flex justify-content-center flex-wrap">
                                <div>
                                    <h5 className="font-weight-normal mt-n4">Karla <span className="font-weight-bold">Giovanna</span></h5>
                                    <small>Diretora Nacional Sênior de Vendas Independente</small>
                                </div>
                                <p className="text-pink mt-3 font-weight-bold text-center">Trabalhando a Unidade pessoal para desenvolver novas líderes e retomar os números</p>
                                <div className="d-flex justify-content-center align-self-end">
                                    <a href="#" className="btn-pink">ACESSAR</a>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>
                </>
            )
        }
        </>
        
    );
}
