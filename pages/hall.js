import Link from 'next/link';
import Header from '../components/Header';
import {Tooltip, OverlayTrigger} from 'react-bootstrap'; 

export default function Index() {

  const renderTooltip = (teste, props) => (
    <Tooltip id="button-tooltip" {...props}>
      {teste}
    </Tooltip>
  );

  return (
  
    <div className="page">
      <Header />

       <svg viewBox="0 0 2000 1062" preserveAspectRatio="none">
        
          <Link href="#">
              <a>
                  <OverlayTrigger
                    placement="left"
                    delay={{ show: 50, hide: 100 }}
                    overlay={renderTooltip("Banner 1")}
                  >
                    <polygon className="polygon" points="296.5,0.5 320.5,341.5 320.5,342.5 496.5,338.5 488.5,117.5 484.5,0.5 "/>
                </OverlayTrigger>
              </a>
          </Link>

          <Link href="#">
              <a>
                <OverlayTrigger
                    placement="left"
                    delay={{ show: 50, hide: 100 }}
                    overlay={renderTooltip("Banner 2")}
                  >
                  <polygon className="polygon" points="641.5,0.5 648.5,237.5 766.5,237.5 760.5,0.5 "/>
                </OverlayTrigger>
              </a>
          </Link>

          <Link href="/produtos">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Produtos")}
                  >
                    <polygon className="polygon" points="678.5,288.5 681.5,385.5 768.5,385.5 766.5,288.5"/>
                  </OverlayTrigger>
                </a>
          </Link>

          <Link href="/orientacoes-mentoras">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Orientações Mentoras")}
                  >
                    <polygon className="polygon" points="692.5,446.5 694.5,546.5 784.5,546.5 782.5,446.5 "/>
                  </OverlayTrigger>
                </a>
              
          </Link>

          <Link href="/plenaria">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Plenária")}
                  >
                    <rect className="polygon" x="942.5" y="442.5"  width="161" height="115"/>
                  </OverlayTrigger>
                </a>
          </Link>

          <Link href="/school-of-life">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("School of Life")}
                  >
                    <polygon className="polygon" points="1275.5,447.5 1273.5,547.5 1364.5,546.5 1366.5,447.5 "/>
                  </OverlayTrigger>
                </a>
          </Link>

          <Link href="/reconhecimentos">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Reconhecimentos")}
                  >
                    <polygon className="polygon" points="1287.5,288.5 1286.5,385.5 1372.5,385.5 1374.5,385.5 1375.5,288.5 "/>
                  </OverlayTrigger>
                </a>           
          </Link>

          <Link href="#">
                <a>
                  <OverlayTrigger
                  placement="left"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Banner 3")}
                  >
                    <polygon className="polygon" points="1290.5,0.5 1284.5,237.5 1404.5,237.5 1410.5,1.5 "/>
                  </OverlayTrigger>
                </a>
          </Link>

          <Link href="#">             
                <a>
                  <OverlayTrigger
                  placement="right"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Banner 4")}
                  >
                    <polygon className="polygon" points="1567.5,0.5 1558.5,226.5 1555.5,337.5 1555.5,339.5 1731.5,342.5 1741.5,192.5 1754.5,1.5 "/>
                  </OverlayTrigger>
                </a>
          </Link>

          <Link href="#">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Frases")}
                  >
                    <path className="polygon" d="M39.5,676.5l17,227l101-18c0,0,5-4,9-4s-3-1,4,0s7,1,7,1l-15-220L39.5,676.5z"/>
                  </OverlayTrigger>
                </a>
          </Link>

          <Link href="#">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Livro")}
                  >
                    <path className="polygon" d="M154.5,663.5l-5-68l134-15h2l9,191l-64,9c0,0-1-1,2,4l15,7l2,30l2,5l-77,15l-12-179L154.5,663.5z"/>      
                  </OverlayTrigger>        
                </a>
              
          </Link>

          <Link href="#">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("????")}
                  >
                    <path className="polygon" d="M419.5,566.5l6,207v2l33-6l5,6v2c0,0-2-1-2,1s-1,20-1,20l87-12l-2-44l-1-42l-1-47l-1-25v-18v-1h6h2l-1-53v-1
                    L419.5,566.5z"/>  
                  </OverlayTrigger>        
                </a>
          </Link>

          <Link href="#">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("???? 2")}
                  >
                    <path className="polygon" d="M542.5,609.5l4,176l1,28h18c0,0,5-4,12-2l85-7v-203l-1-1L542.5,609.5z"/>      
                  </OverlayTrigger>       
                </a>
          </Link>

          <Link href="#">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Calendário")}
                  >
                    <polygon className="polygon" points="1402.5,641.5 1404.5,732.5 1444.5,737.5 1446.5,644.5 "/> 
                  </OverlayTrigger>             
                </a>
          </Link>

          <Link href="#">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Banner 5")}
                  >
                    <polygon className="polygon" points="1515.5,617.5 1515.5,738.5 1516.5,738.5 1516.5,867.5 1516.5,868.5 1657.5,883.5 1657.5,622.5 "/>     
                </OverlayTrigger>        
                </a>
          </Link>

          <Link href="#">
                <a>
                  <OverlayTrigger
                  placement="top"
                  delay={{ show: 50, hide: 100 }}
                  overlay={renderTooltip("Banner 6")}
                  >
                    <polygon className="polygon" points="1759.5,588.5 1757.5,839.5 1897.5,856.5 1899.5,856.5 1902.5,596.5 "/>   
                  </OverlayTrigger>           
                </a>
          </Link>

        </svg>

      <img src="/images/MK_HALL.jpg" className="img-background" />
    </div>
  )


}

