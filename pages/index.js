import { useEffect } from 'react'
import { useRouter } from 'next/router'

export default function Auth() {
    const router = useRouter()

    useEffect(() => {
        if(router.query.string) {
            const credentials = router.query.string
            localStorage.setItem('credentials', credentials.replace(/\s/g, '+'))
            router.push('/hall')
        } else {
            router.push('/noauth')
        }
      }, [router.query.string])

    return (
        <div>
            
        </div>
    )
}